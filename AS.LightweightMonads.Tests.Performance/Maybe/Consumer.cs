﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Diagnosers;
using static LanguageExt.Prelude;

namespace AS.LightweightMonads.Tests.Performance.Maybe
{
    [HtmlExporter, 
     MarkdownExporterAttribute.Default,
    MemoryDiagnoser,
    HardwareCounters(
        HardwareCounter.BranchInstructions, 
        HardwareCounter.BranchMispredictions,
        HardwareCounter.CacheMisses,
        HardwareCounter.InstructionRetired,
        HardwareCounter.LlcReference,
        HardwareCounter.LlcMisses,
        HardwareCounter.TotalCycles,
        HardwareCounter.BranchMispredictsRetired,
        HardwareCounter.TotalIssues)]
    public class Consumer
    {
        Service service;

        public Consumer()
        {
            this.service = new Service();
        }

        [Benchmark]
        public string LXOptionFlowA()
        {
            return service.LX_Step1()
                .Bind(service.LX_Step2)
                .Bind(service.LX_Step3a)
                .Bind(service.LX_Step4)
                .Match(identity, "");
        }

        [Benchmark]
        public string LXOptionFlowB()
        {
            return service.LX_Step1()
                .Bind(service.LX_Step2)
                .Bind(service.LX_Step3b)
                .Bind(service.LX_Step4)
                .Match(identity, "");
        }

        [Benchmark]
        public string MaybeFlowA()
        {
            return service.M_Step1()
                .Bind(service.M_Step2)
                .Bind(service.M_Step3a)
                .Bind(service.M_Step4)
                .ValueOrDefault("");
        }

        [Benchmark]
        public string MaybeFlowB()
        {
            return service.M_Step1()
                .Bind(service.M_Step2)
                .Bind(service.M_Step3b)
                .Bind(service.M_Step4)
                .ValueOrDefault("");
        }

        [Benchmark]
        public string NullableFlowA()
        {
            var res = service.Nb_Step1();
            if (res == null) return "";

            var res2 = service.Nb_Step2(res.Value);
            if (res2 == null) return "";

            var res3 = service.Nb_Step3a(res2.Value);
            if (res3 == null) return "";

            var res4 = service.Nb_Step4(res3.Value);
            return res4 ?? "";
        }

        [Benchmark]
        public string NullableFlowB()
        {
            var res = service.Nb_Step1();
            if (res == null) return "";

            var res2 = service.Nb_Step2(res.Value);
            if (res2 == null) return "";

            var res3 = service.Nb_Step3b(res2.Value);
            if (res3 == null) return "";

            var res4 = service.Nb_Step4(res3.Value);
            return res4 ?? "";
        }

        [Benchmark]
        public string TryFlowA()
        {
            var (res, v) = service.Try_Step1();
            if (!res) return "";

            var (res2, v2) = service.Try_Step2(v);
            if (!res2) return "";

            var (res3, v3) = service.Try_Step3a(v2);
            if (!res3) return "";

            var (res4, v4) = service.Try_Step4(v3);
            return res4 ? v4 : "";
        }

        [Benchmark]
        public string TryFlowB()
        {
            var (res, v) = service.Try_Step1();
            if (!res) return "";

            var (res2, v2) = service.Try_Step2(v);
            if (!res2) return "";

            var (res3, v3) = service.Try_Step3b(v2);
            if (!res3) return "";

            var (res4, v4) = service.Try_Step4(v3);
            return res4 ? v4 : "";
        }

        [Benchmark]
        public string ExFlowA()
        {
            try
            {
                return service.Ex_Step4(
                    service.Ex_Step3a(
                        service.Ex_Step2(
                            service.Ex_Step1())));
            }
            catch
            {
                return "";
            }
        }

        [Benchmark]
        public string ExFlowB()
        {
            try
            {
                return service.Ex_Step4(
                    service.Ex_Step3b(
                        service.Ex_Step2(
                            service.Ex_Step1())));
            }
            catch
            {
                return "";
            }
        }
    }
}