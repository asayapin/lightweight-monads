﻿using System;
using System.Globalization;
using LanguageExt;

namespace AS.LightweightMonads.Tests.Performance.Maybe
{
    public class Service
    {
        public Maybe<int> M_Step1() => new Random().Next(64);
        public Maybe<bool> M_Step2(int v) => v % 2 == 0 ? v > 31 : Maybe<bool>.None;
        public Maybe<decimal> M_Step3a(bool v) => v ? Maybe<decimal>.None : new Random().Next(64);
        public Maybe<decimal> M_Step3b(bool v) => Maybe<decimal>.None;
        public Maybe<string> M_Step4(decimal v) => v.ToString(CultureInfo.InvariantCulture);
        
        
        public (bool, int) Try_Step1() => (true, new Random().Next(64));
        public (bool, bool) Try_Step2(int v) => (v % 2 == 0, v > 31);
        public (bool, decimal) Try_Step3a(bool v) => (!v, new Random().Next(v ? 32 : 64));
        public (bool, decimal) Try_Step3b(bool v) => (false, 123m); 
        public (bool, string) Try_Step4(decimal v) => (true, v.ToString(CultureInfo.InvariantCulture));
        
        public int Ex_Step1() => new Random().Next(64);
        public bool Ex_Step2(int v) => v % 2 == 0 ? v > 31 : throw new Exception();
        public decimal Ex_Step3a(bool v) => v ? throw new Exception() : new Random().Next(64);
        public decimal Ex_Step3b(bool _) => throw new Exception();
        public string Ex_Step4(decimal v) => v.ToString(CultureInfo.InvariantCulture);
        
        public Option<int> LX_Step1() => new Random().Next(64);
        public Option<bool> LX_Step2(int v) => v % 2 == 0 ? v > 31 : Option<bool>.None;
        public Option<decimal> LX_Step3a(bool v) => v ? Option<decimal>.None : new Random().Next(64);
        public Option<decimal> LX_Step3b(bool v) => Option<decimal>.None;
        public Option<string> LX_Step4(decimal v) => v.ToString(CultureInfo.InvariantCulture);
        
        public int? Nb_Step1() => new Random().Next(64);
        public bool? Nb_Step2(int v) => v % 2 == 0 ? v > 31 : (bool?)null;
        public decimal? Nb_Step3a(bool v) => v ? (decimal?)null : new Random().Next(64);
        public decimal? Nb_Step3b(bool v) => null;
        public string Nb_Step4(decimal v) => v.ToString(CultureInfo.InvariantCulture);
    }
}