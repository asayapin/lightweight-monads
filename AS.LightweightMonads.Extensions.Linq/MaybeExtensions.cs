﻿using System;

namespace AS.LightweightMonads.Extensions.Linq
{
    public static class MaybeExtensions
    {
        public static Maybe<R> Select<T, R>(this Maybe<T> m, Func<T, R> map) => m.Map(map);

        public static Maybe<T> Where<T>(this Maybe<T> m, Predicate<T> predicate) =>
            m.IsSome && predicate(m.UnsafeValue) ? m : Maybe<T>.None;

        public static Maybe<R> SelectMany<T, S, R>(this Maybe<T> m, Func<T, Maybe<S>> second, Func<T, S, R> result)
        {
            if (m.IsNone) return Maybe<R>.None;
            var s = second(m.UnsafeValue);
            if (s.IsNone) return Maybe<R>.None;
            return Maybe<R>.From(result(m.UnsafeValue, s.UnsafeValue));
        }
    }
}