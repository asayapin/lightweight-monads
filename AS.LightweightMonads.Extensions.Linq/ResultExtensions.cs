using System;

namespace AS.LightweightMonads.Extensions.Linq
{
    public static class ResultExtensions
    {
        public static Result<U, L> Select<R, L, U>(this Result<R, L> m, Func<R, U> map) => m.Map(map);

        public static Result<R, L> Where<R, L>(this Result<R, L> result, Predicate<R> p) =>
            result.IsOk && !p(result.UnsafeOkValue)
                ? default(L)
                : result;

        public static Result<R3, L> SelectMany<R, L, R2, R3>(this Result<R, L> m, Func<R, Result<R2, L>> second, Func<R, R2, R3> result)
        {
            if (m.IsError) return m.UnsafeErrorValue;
            var s = second(m.UnsafeOkValue);
            if (s.IsError) return s.UnsafeErrorValue;
            return result(m.UnsafeOkValue, s.UnsafeOkValue);
        }
    }
}