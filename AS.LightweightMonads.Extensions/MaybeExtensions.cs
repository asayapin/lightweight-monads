﻿using System;
using System.Linq;

namespace AS.LightweightMonads.Extensions
{
    public static class MaybeExtensions
    {
        public static R Match<T, R>(this Maybe<T> m, Func<T, R> some, Func<R> none) => m.IsNone ? none() : some(m.UnsafeValue);

        public static Maybe<R> Map<T, R>(this Maybe<T> m, Func<T, R> map) => m.Bind(v => Maybe<R>.From(map(v)));

        public static void Iter<T>(this Maybe<T> m, Action<T> action)
        {
            if (m.IsSome) action(m.UnsafeValue);
        }

        public static Maybe<T> Execute<T>(this Maybe<T> m, Action<T> action)
        {
            if (m.IsSome) action(m.UnsafeValue);
            return m;
        }
    }
}