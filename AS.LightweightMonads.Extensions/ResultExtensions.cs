using System;

namespace AS.LightweightMonads.Extensions
{
    public static class ResultExtensions
    {
        public static U Match<R, L, U>(this Result<R, L> result, Func<R, U> onOk, Func<L, U> onError) =>
            result.IsOk
                ? onOk(result.UnsafeOkValue)
                : onError(result.UnsafeErrorValue);

        public static Result<U, L> Map<R, L, U>(this Result<R, L> result, Func<R, U> mapper) =>
            result.IsOk
                ? mapper(result.UnsafeOkValue)
                : result.UnsafeErrorValue;
        public static Result<R, U> Map<R, L, U>(this Result<R, L> result, Func<L, U> mapper) =>
            result.IsError
                ? mapper(result.UnsafeErrorValue)
                : result.UnsafeOkValue;
        public static Result<U, V> Map<R, L, U, V>(this Result<R, L> result, Func<R, U> mapOk, Func<L, V> mapErr) =>
            result.IsOk
                ? mapOk(result.UnsafeOkValue)
                : mapErr(result.UnsafeErrorValue);

        public static void Iter<R, L>(this Result<R, L> result, Action<R> action)
        {
            if (result.IsOk) action(result.UnsafeOkValue);
        }
        public static void Iter<R, L>(this Result<R, L> result, Action<L> action)
        {
            if (result.IsError) action(result.UnsafeErrorValue);
        }
        public static void Iter<R, L>(this Result<R, L> result, Action<R> actOk, Action<L> actErr)
        {
            if (result.IsOk) actOk(result.UnsafeOkValue);
            else actErr(result.UnsafeErrorValue);
        }

        public static Result<R, L> Execute<R, L>(this Result<R, L> result, Action<R> action)
        {
            if (result.IsOk) action(result.UnsafeOkValue);
            return result;
        }
        public static Result<R, L> Execute<R, L>(this Result<R, L> result, Action<L> action)
        {
            if (result.IsError) action(result.UnsafeErrorValue);
            return result;
        }
        public static Result<R, L> Execute<R, L>(this Result<R, L> result, Action<R> actOk, Action<L> actErr)
        {
            if (result.IsOk) actOk(result.UnsafeOkValue);
            else actErr(result.UnsafeErrorValue);
            return result;
        }
    }
}