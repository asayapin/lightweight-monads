﻿using System;

namespace AS.LightweightMonads
{
    public abstract class Maybe<T>
    {
        public static Maybe<T> None { get; } = new Empty();
        public static Maybe<T> From(T t) => Equals(t, default(T)) ? (Maybe<T>)new Empty() : new Some(t);
        public static Maybe<N> FromNullable<N>(N? t) where N : struct => t.HasValue ? (Maybe<N>)new Maybe<N>.Some(t.Value) : new Maybe<N>.Empty();

        public static implicit operator Maybe<T>(T value) => From(value);
        
        public abstract T UnsafeValue { get; }
        public T ValueOrDefault(T defaultValue) => IsNone ? defaultValue : UnsafeValue;
        
        public abstract bool IsNone { get; }
        public bool IsSome => !IsNone;
        
        public abstract Maybe<R> Bind<R>(Func<T, Maybe<R>> binder);

        private Maybe(){}
        
        private class Empty : Maybe<T>
        {
            public override T UnsafeValue => throw new NullReferenceException();
            public override bool IsNone => true;
            public override Maybe<R> Bind<R>(Func<T, Maybe<R>> binder) => new Maybe<R>.Empty();
        }

        private class Some : Maybe<T>
        {
            readonly T value;
            public Some(T value)
            {
                this.value = value;
            }

            public override T UnsafeValue => value;
            public override bool IsNone => false;
            public override Maybe<R> Bind<R>(Func<T, Maybe<R>> binder) => binder(value);
        }
    }
}