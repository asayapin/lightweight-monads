using System;

namespace AS.LightweightMonads
{
    public abstract class Result<TOk, TErr>
    {
        private Result() { }

        public static Result<TOk, TErr> Ok(TOk o) => new Right(o);
        public static Result<TOk, TErr> Error(TErr o) => new Left(o);

        public static implicit operator Result<TOk, TErr>(TOk o) => Ok(o);
        public static implicit operator Result<TOk, TErr>(TErr o) => Error(o);

        public abstract bool IsOk { get; }
        public bool IsError => !IsOk;

        public abstract TOk UnsafeOkValue { get; }
        public abstract TErr UnsafeErrorValue { get; }

        public abstract Result<R, TErr> Bind<R>(Func<TOk, Result<R, TErr>> binder);

        private class Right : Result<TOk, TErr>
        {
            private readonly TOk value;
            public Right(TOk data) => value = data;

            public override bool IsOk => true;

            public override Result<R, TErr> Bind<R>(Func<TOk, Result<R, TErr>> binder) => binder(value);

            public override bool Equals(object obj) => obj is Right ok && value.Equals(ok.value);

            public override int GetHashCode() => value.GetHashCode();

            public override TErr UnsafeErrorValue => throw new InvalidOperationException();

            public override TOk UnsafeOkValue => value;
        }
        private class Left : Result<TOk, TErr>
        {
            private readonly TErr value;
            public Left(TErr data) => value = data;

            public override bool IsOk => false;

            public override Result<R, TErr> Bind<R>(Func<TOk, Result<R, TErr>> binder) => new Result<R, TErr>.Left(value);

            public override bool Equals(object obj) => obj is Left ok && Equals(value, ok.value);

            public override int GetHashCode() => value.GetHashCode();

            public override TErr UnsafeErrorValue => value;

            public override TOk UnsafeOkValue => throw new InvalidOperationException();
        }
    }
}