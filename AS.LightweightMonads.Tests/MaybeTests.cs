using System;
using System.Linq;
using AS.LightweightMonads.Extensions;
using AS.LightweightMonads.Extensions.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace AS.LightweightMonads.Tests
{

    public class MaybeTests
    {
        public readonly Maybe<int>
            of5 = Maybe<int>.From(5),
            of7 = Maybe<int>.From(7),
            ofNull = Maybe<int>.FromNullable<int>(null);

        private static Maybe<T> Check<T>(Maybe<T> m, Predicate<T> p) => from value in m where p(value) select value;

        private static Maybe<R> CheckMany<T, S, R>(Maybe<T> m1, Maybe<S> m2, Func<T, S, R> f) =>
            from v1 in m1
            from v2 in m2
            select f(v1, v2);

        private static void Iterate<T>(Maybe<T> m, Action<T> a) => m.Iter(a);

        [Test]
        public void TestQuerySyntaxForSingleValue()
        {
            Check(ofNull, i => i > 6).IsNone.Should().BeTrue();
            Check(of5, i => i > 6).IsNone.Should().BeTrue();
            Check(of7, i => i > 6).IsNone.Should().BeFalse();
        }

        [Test]
        public void TestQuerySyntaxForMultipleValues()
        {
            CheckMany(of5, of7, (i, i1) => i + i1).IsSome.Should().BeTrue();
            CheckMany(of5, ofNull, (i, i1) => i + i1).IsNone.Should().BeTrue();
            CheckMany(ofNull, of7, (i, i1) => i + i1).IsNone.Should().BeTrue();
        }

        [Test]
        public void TestBasicExpectations()
        {
            of5.IsNone.Should().BeFalse();
            of5.IsSome.Should().BeTrue();
            of5.UnsafeValue.Should().Be(5);
            of5.ValueOrDefault(0).Should().Be(5);

            ofNull.IsSome.Should().BeFalse();
            ofNull.IsNone.Should().BeTrue();
            Assert.Throws<NullReferenceException>(() =>
            {
                var _ = ofNull.UnsafeValue;
            });
            ofNull.ValueOrDefault(1).Should().Be(1);

            ofNull.Bind<int>(x => x + 3).IsNone.Should().BeTrue();
            var plus3 = of5.Bind<int>(x => x + 3);
            plus3.UnsafeValue.Should().Be(8);
        }

        [Test]
        public void TestIterMapMatchExtensions()
        {
            Iterate(of5, v => v.Should().BePositive());
            Iterate(ofNull, v => throw new Exception());

            of5.Map(x => x % 2 == 0).Iter(x => x.Should().BeFalse());

            of5.Match(x => x % 2 == 1, () => false).Should().BeTrue();
            ofNull.Match(x => x % 2 == 1, () => false).Should().BeFalse();
        }
    }
}