using System;
using System.Collections.Generic;
using System.Linq;
using AS.LightweightMonads.Extensions;
using AS.LightweightMonads.Extensions.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace AS.LightweightMonads.Tests
{
    public class ResultTests
    {
        private static readonly Result<int, string>
            of5 = 5,
            of7 = 7,
            ofNone = "none";

        [Test]
        public void TestBasicExpectations()
        {
            of5.IsOk.Should().BeTrue();
            of5.IsError.Should().BeFalse();

            ofNone.IsError.Should().BeTrue();
            ofNone.IsOk.Should().BeFalse();

            of5.UnsafeOkValue.Should().Be(5);
            ofNone.UnsafeErrorValue.Should().Be("none");

            Assert.Throws<InvalidOperationException>(() => { var _ = of5.UnsafeErrorValue; });
            Assert.Throws<InvalidOperationException>(() => { var _ = ofNone.UnsafeOkValue; });

            of5.Bind<bool>(x => true).UnsafeOkValue.Should().BeTrue();
            ofNone.Bind<bool>(x => true).UnsafeErrorValue.Should().Be("none");
        }

        [Test]
        public void TestMatchExtension()
        {
            of5.Match(x => x % 2 == 0, x => x.Length % 2 == 0).Should().BeFalse();
            ofNone.Match(x => x % 2 == 0, x => x.Length % 2 == 0).Should().BeTrue();
        }

        [Test]
        public void TestMapExtension()
        {
            Func<int, bool> eveni = i => i % 2 == 0;
            Func<string, bool> evens = s => eveni(s.Length);

            var o51 = of5.Map(eveni);
            var o52 = of5.Map(evens);
            var o53 = of5.Map(eveni, evens);

            var on1 = ofNone.Map(eveni);
            var on2 = ofNone.Map(evens);
            var on3 = ofNone.Map(eveni, evens);

            new[] { o51.IsOk, o52.IsOk, o53.IsOk }.All(r => r).Should().BeTrue();
            new[] { o51.UnsafeOkValue, o53.UnsafeOkValue }.All(x => !x).Should().BeTrue();
            o52.UnsafeOkValue.Should().Be(5);

            new[] { on1.IsError, on2.IsError, on3.IsError }.All(r => r).Should().BeTrue();
            new[] { on2.UnsafeErrorValue, on3.UnsafeErrorValue }.All(x => x).Should().BeTrue();
            on1.UnsafeErrorValue.Should().Be("none");
        }

        [Test]
        public void TestIterExecuteExtension()
        {
            of5.Iter((int x) => x.Should().Be(5));
            of5.Iter((string x) => Assert.Fail());
            of5.Iter(x => x.Should().Be(5), (string x) => Assert.Fail());

            ofNone.Iter((string x) => x.Should().Be("none"));
            ofNone.Iter((int x) => Assert.Fail());
            ofNone.Iter(x => Assert.Fail(), x => x.Should().Be("none"));

            of5.Execute((int x) => x.Should().Be(5)).Equals(of5).Should().BeTrue();
            of5.Execute((string x) => Assert.Fail()).Equals(of5).Should().BeTrue();
            of5.Execute(x => x.Should().Be(5), (string x) => Assert.Fail()).Equals(of5).Should().BeTrue();

            ofNone.Execute((string x) => x.Should().Be("none")).Equals(ofNone).Should().BeTrue();
            ofNone.Execute((int x) => Assert.Fail()).Equals(ofNone).Should().BeTrue();
            ofNone.Execute(x => Assert.Fail(), x => x.Should().Be("none")).Equals(ofNone).Should().BeTrue();
        }

        public static IEnumerable<TestCaseData> WhereCases()
        {
            Predicate<int> eq5 = x => x == 5;
            Predicate<int> ne5 = x => x != 5;
            yield return new TestCaseData(of5, eq5, of5).SetName("5 == 5");
            yield return new TestCaseData(of5, ne5, Result<int, string>.Error(null)).SetName("5 != 5");
            yield return new TestCaseData(ofNone, eq5, ofNone).SetName("none == 5");
            yield return new TestCaseData(ofNone, ne5, ofNone).SetName("none != 5");
        }

        public static IEnumerable<TestCaseData> SelectCases()
        {
            Func<int, int> add5 = x => x + 5;
            yield return new TestCaseData(of5, add5, Result<int, string>.Ok(10)).SetName("5 + 5");
            yield return new TestCaseData(ofNone, add5, ofNone).SetName("none + 5");
        }

        public static IEnumerable<TestCaseData> SelectManyCases()
        {
            Func<int, int, int> add = (x, y) => x + y;
            yield return new TestCaseData(of5, of7, add, Result<int, string>.Ok(12)).SetName("5 + 7");
            yield return new TestCaseData(ofNone, of5, add, ofNone).SetName("none + 5");
            yield return new TestCaseData(of5, ofNone, add, ofNone).SetName("5 + none");
            yield return new TestCaseData(ofNone, ofNone, add, ofNone).SetName("none + none");
        }

        [TestCaseSource(nameof(WhereCases))]
        public void TestQueryWhere(Result<int, string> r, Predicate<int> p, Result<int, string> exp)
        {
            var res =
                from x in r
                where p(x)
                select x;

            res.Equals(exp).Should().BeTrue();
        }

        [TestCaseSource(nameof(SelectCases))]
        public void TestQuerySelect(Result<int, string> result, Func<int, int> mapper, Result<int, string> exp)
        {
            (from x in result
             select mapper(x))
            .Equals(exp).Should().BeTrue();
        }

        [TestCaseSource(nameof(SelectManyCases))]
        public void TestSelectManyResult(Result<int, string> f, Result<int, string> s, Func<int, int, int> map, Result<int, string> exp)
        {
            (from fi in f
             from si in s
             select map(fi, si))
            .Equals(exp).Should().BeTrue();
        }
    }
}