dotnet new nugetconfig --force
dotnet nuget add source "$CI_SERVER_URL/api/v4/projects/$CI_PROJECT_ID/packages/nuget/index.json" -n pkgfeed -u gitlab-ci-token -p "$CI_JOB_TOKEN" --store-password-in-clear-text --configfile nuget.config
dotnet nuget push "**/*.nupkg" -k az -s pkgfeed --skip-duplicate